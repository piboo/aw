package com.test;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class KafkaTest {
	final static String TOPIC = "my-topic-piero";
	final static String KAFKA_HOST = "localhost";
	final static String KAFKA_BROKER_LIST_PORT = "9092";
	final static String KAFKA_CONSUMER_PORT = "2181";
	
	public static void main(String[] argv) throws UnsupportedEncodingException {

		List<String> messages= new ArrayList<String>();
		messages.add("{id:1, content:\"message one\"}");
		messages.add("{id:2, content:\"message two\"}");
		messages.add("{id:3, content:\"message three\"}");

		KafkaProducer kafkaProducer = new KafkaProducer(KAFKA_HOST+":"+KAFKA_BROKER_LIST_PORT,TOPIC);
		kafkaProducer.initialize();
		kafkaProducer.sendMessage(messages);

		Executor executor = Executors.newSingleThreadExecutor();
		executor.execute( new KafkaConsumer(KAFKA_HOST+":"+KAFKA_CONSUMER_PORT, TOPIC));


	}
}
