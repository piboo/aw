package com.test;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;

public class KafkaConsumer implements Runnable {
    final static String clientId = "SimpleConsumer";
    
    private String topic;
    private String zookeperAddress;
   
    ConsumerConnector consumerConnector;
    
    /**
     * Constructor
     * 
     * param: zookeperAddress the zookeper Address
     * param: topic the name of the topic 
     */
    public KafkaConsumer(String zookeperAddress, String topic){
       
    	this.zookeperAddress = zookeperAddress;
        this.topic = topic;
       
    }
    
    /**
     * initialise the object;
     */
    private void initialize(){
    	 Properties properties = new Properties();
         properties.put("zookeeper.connect",zookeperAddress);
         properties.put("group.id","test-group");
         
         ConsumerConfig consumerConfig = new ConsumerConfig(properties);
         this.consumerConnector = Consumer.createJavaConsumerConnector(consumerConfig);
    }

    
    
    @Override
    public void run() {
    	// initialize the object 
    	this.initialize();
    	
        Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(this.topic, new Integer(1));
        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = this.consumerConnector.createMessageStreams(topicCountMap);
        KafkaStream<byte[], byte[]> stream =  consumerMap.get(this.topic).get(0);
        ConsumerIterator<byte[], byte[]> it = stream.iterator();
        while(it.hasNext()) {
            System.out.println(new String(it.next().message()));
        }    
   
    }
    

}