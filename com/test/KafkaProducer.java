package com.test;


import java.util.List;
import java.util.Properties;

import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;


public class KafkaProducer {
    private String topic;
    
    private String brokerList;
    
    private kafka.javaapi.producer.Producer<String,String> producer;
    
    /**
     * 
     * @param brokerList
     * @param topic
     */
    public KafkaProducer(String brokerList,String topic ){
    	
    	this.brokerList = brokerList;
    	this.topic = topic;
 
    }
    
    /**
     * initialize the object
     */
    public void initialize() {
    	Properties properties = new Properties();
    	properties.put("metadata.broker.list",brokerList);
        properties.put("serializer.class","kafka.serializer.StringEncoder");
        ProducerConfig producerConfig = new ProducerConfig(properties);
        this.producer = new kafka.javaapi.producer.Producer<String, String>(producerConfig);
    }
    
    /**
     * 
     * @param messages
     */
    public void sendMessage(List<String> messages){
        
    	for (String message : messages) {
    		KeyedMessage<String, String> keyedMessage = new KeyedMessage<String, String>(this.topic,message);
    		producer.send(keyedMessage);	
		}
       
        producer.close();
    }
}